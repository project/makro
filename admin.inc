<?php

/**
 * @file
 * Admin functions.
 */

/**
 * Settings form.
 */
function makro_admin_settings() {
  return system_settings_form($form);
}

/**
 * Page callback.
 */
function makro_page($op) {
  $output = '';
  switch ($op) {
    case 'start':
      variable_set('makro_enabled', TRUE);
      drupal_set_message(t('Recording started.'));
      break;

    case 'stop':
      variable_set('makro_enabled', FALSE);
      drupal_set_message(t('Recording stopped.'));
      break;

    case 'clear':
      db_query('DELETE FROM {makro}');
      drupal_set_message(t('Deleted all recordings.'));
      break;

  }
  return $output;
}

/**
 * A form callback that displays the macro import form.
 *
 * @return
 *   Form for importing a previously recorded macro.
 */
function makro_play_form() {
  $form['macro'] = array(
    '#type' => 'textarea',
    '#title' => t('Macro to import'),
    '#rows' => 20,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Play macro'),
  );
  return $form;
}

function makro_play_form_submit($form, &$form_state) {
  eval($form_state['values']['macro']);
  $cookie_file = tempnam('', '');
  $previous_result = NULL;
  foreach ($makro as $key => $data) {
    $previous_result = makro_play($data, $cookie_file, $previous_result);
  }
  drupal_set_message(t('Macro was executed.'));
  @unlink($cookie_file);
}


function makro_export_page() {
  $form['code'] = array(
    '#type' => 'textarea',
    '#title' => t('Exported macros'),
    '#default_value' => _makro_get(),
    '#rows' => 20,
  );
  return $form;

}
